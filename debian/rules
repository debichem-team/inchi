#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/dpkg/buildtools.mk

SOMAJOR = $(shell grep -P '^\s*MAIN_VERSION' INCHI-1-SRC/INCHI_API/libinchi/gcc/makefile | cut -d . -f 2)
SOMINOR = $(shell grep -P '^\s*VERSION' INCHI-1-SRC/INCHI_API/libinchi/gcc/makefile | cut -d . -f 2-)

SOVERSION = $(SOMAJOR).$(SOMINOR)

SHARED_LINK_PARM = $(shell dpkg-buildflags --get LDFLAGS)

%:
	dh $@ --buildsystem makefile

override_dh_auto_build:
	mkdir --parents INCHI-1-SRC/INCHI_API/bin/Linux
	mkdir --parents INCHI-1-SRC/INCHI_EXE/bin/Linux
	# Building inchi_main builds shared library too
	dh_auto_build -- -C INCHI-1-SRC/INCHI_API/demos/inchi_main/gcc SHARED_LINK_PARM="$(SHARED_LINK_PARM)"
	dh_auto_build -- -C INCHI-1-SRC/INCHI_EXE/inchi-1/gcc SHARED_LINK_PARM="$(SHARED_LINK_PARM)" C_COMPILER='$(CC)' CPP_COMPILER='$(CXX)' LINKER='$(CXX) -s'

execute_after_dh_install:
	find INCHI-1-SRC/INCHI_API/bin/Linux -name 'libinchi.so.*' \
		| grep -v '.gz$$' \
		| xargs -i dh_install -plibinchi$(SOVERSION) {} usr/lib/$(DEB_HOST_MULTIARCH)
	mkdir --parents $(CURDIR)/debian/libinchi-dev/usr/lib/$(DEB_HOST_MULTIARCH)
	ln -s libinchi.so.$(SOVERSION) \
		$(CURDIR)/debian/libinchi-dev/usr/lib/$(DEB_HOST_MULTIARCH)/libinchi.so

override_dh_installman: debian/inchi_main.1 debian/inchi-1.1
	dh_installman

override_dh_auto_clean:
	find $(CURDIR) -name "*.o" -delete
	$(RM) INCHI-1-SRC/INCHI_API/libinchi/gcc/*.so. \
	      INCHI-1-SRC/INCHI_API/libinchi/gcc/inchi_main
	$(RM) -rf INCHI-1-SRC/INCHI_API/bin
	$(RM) -rf INCHI-1-SRC/INCHI_EXE/bin

debian/%.1: debian/%.1.txt
	a2x -f manpage $<
