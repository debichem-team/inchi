inchi-1(1)
==========

Name
----
inchi-1 - inchi-1 executable

Synopsis
--------
'inchi-1 inputFile [outputFile [logFile [problemFile]]] [-option[ -option...]]'
'inchi-1 inputFiles... -AMI [-option[ -option...]]'

Options
-------

.Input
- STDIO:       Use standard input/output streams
- InpAux:      Input structures in InChI default aux. info format (for use with STDIO)
- SDF:DataHeader: Read from the input SDfile the ID under this DataHeader
- START:n:     Start at n-th input structure
- END:n:       Stop after n-th input structure
- RECORD:n:    Treat only n-th input structure
- AMI:         Allow multiple input files (wildcards supported)
- AMIOutStd:   Write output to stdout (in AMI mode)
- AMILogStd:   Write log to stderr (in AMI mode)
- AMIPrbNone:  Suppress creation of problem files (in AMI mode)

.Output
- NoLabels:    Omit structure number, DataHeader and ID from InChI output
- NoWarnings:  Suppress all warning messages
- AuxNone:     Omit auxiliary information
- SaveOpt:     Save custom InChI creation options (non-standard InChI)
- Tabbed:      Separate structure number, InChI, and AuxInfo with tabs
- MergeHash:   Combine InChIKey with extra hash(es) if present
- NoInChI:     Do not print InChI string itself
- OutErrInChI: On fail, print empty InChI (default: nothing)
- OutputSDF:   Convert InChI created with default aux. info to SDfile
- SdfAtomsDT:  Output Hydrogen Isotopes to SDfile as Atoms D and T

.Structure perception
- SNon:        Exclude stereo (default: include absolute stereo)
- NEWPSOFF:    Both ends of wedge point to stereocenters (default: a narrow end)
- LooseTSACheck:   Relax criteria of ambiguous drawing for in-ring tetrahedral stereo
- DoNotAddH:   All H are explicit (default: add H according to usual valences)

.Stereo perception modifiers (non-standard InChI)
- SRel:        Relative stereo
- SRac:        Racemic stereo
- SUCF:        Use Chiral Flag: On means Absolute stereo, Off - Relative

.Customizing InChI creation (non-standard InChI)
- SUU:         Always include omitted unknown/undefined stereo
- SLUUD:       Make labels for unknown and undefined stereo different
- RecMet:      Include reconnected metals results
- FixedH:      Include Fixed H layer
- KET:         Account for keto-enol tautomerism (experimental)
- 15T:         Account for 1,5-tautomerism (experimental)
- PT_22_00:    Account for PT_22_00 tautomerism (experimental)
- PT_16_00:    Account for PT_16_00 tautomerism (experimental)
- PT_06_00:    Account for PT_06_00 tautomerism (experimental)
- PT_39_00:    Account for PT_39_00 tautomerism (experimental)
- PT_13_00:    Account for PT_13_00 tautomerism (experimental)
- PT_18_00:    Account for PT_18_00 tautomerism (experimental)

.Generation
- Wnumber:     Set time-out per structure in seconds; W0 means unlimited
- WMnumber:    Set time-out per structure in milliseconds (int); WM0 means unlimited
- LargeMolecules: Treat molecules up to 32766 atoms (experimental)
- WarnOnEmptyStructure: Warn and produce empty InChI for empty structure
- Polymers:    Allow processing of polymers (experimental)
- Polymers105: Allow processing of polymers (experimental, legacy mode of v. 1.05)
- FoldCRU:     Fold polymer CRU if inner repeats occur
- NoFrameShift: Disable polymer CRU frame shift
- NoEdits:     Disable polymer CRU frame shift and folding
- NPZz:        Allow non-polymer-related Zz atoms (pseudo element placeholders)
- SAtZz:       Allow stereo at atoms connected to Zz(default: disabled)
- Key:         Generate InChIKey
- XHash1:      Generate hash extension (to 256 bits) for 1st block of InChIKey
- XHash2:      Generate hash extension (to 256 bits) for 2nd block of InChIKey

.Conversion
- InChI2Struct: Convert InChI string(s) to structure(s) in InChI aux.info format
- InChI2InChI:  Convert  Convert InChI string(s) into InChI string(s)
